import pytest
import sys
import os
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../src") # To run the unitest on Linux/Windows

from backall import cli


file_path = os.getcwd()

#Este decorador es usado si quiero eliminar cada parser=cli.create_parser de las funciones de abajo, por ahora no lo usare
"""@pytest.fixture()
def parser():
    return cli.create_parser()
"""

def test_without_file():
    """
    It will exit if dont receive the correct cloud service
    """
    with pytest.raises(SystemExit):
        parser = cli.create_parser()  #This cli module and creat_parser function will be created later
        parser.parse_args([file_path])


def test_with_service():
    """
    It will exit if it receives a service name but not a destination
    """
    parser = cli.create_parser()

    with pytest.raises(SystemExit):
        parser.parse_args([file_path, "--service", "s3"])

def test_with_service_destination():
    """
    It will NOT exit if receives a cloud service and a destination
    
    """
    parser = cli.create_parser()
    args = parser.parse_args([file_path, "--service", "s3", "/bucket/name"])

    assert args.file_path == file_path
    assert args.service == "s3"
    assert args.destination == "/bucket/name" 

