﻿backall
========

CLI for backing up regular files to AWS S3.

Requirements
-------------------------

1. Install ``pip`` and ``pipenv`` 
2. Clone repository: ``git clone git@github.com:heagma/backall``
3. ``cd`` into backall
4. Fetch development dependencies ``make install``
5. Activate virtualenv: ``pipenv shell``

How to use it
-----

Pass in a file directory, cloud service(S3) and destination (Bucket name).

Example:

::

    $ backall my/file/path -d s3 bucket-name



Running Tests
-------------

Run tests locally using ``make`` if virtualenv is active:

::

    $ make

If virtualenv isn’t active then use:

::

    $ pipenv run make