from setuptools import setup, find_packages

with open("README.rst", 'r') as f:
    long_description = f.read()

setup(
   name='backall',
   version='0.1.0',
   description='Back up files to S3 AWS',
   license='MIT',
   long_description=long_description,
   author='HeAgMa',
   author_email='heagma@live.com',
   url='https://gitlab.com/heagma/backall',
   packages=find_packages('src'),
   package_dir={'': 'src'},
   install_requires=[], #external packages as dependencies
)
