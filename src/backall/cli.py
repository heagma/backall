from argparse import Action, ArgumentParser

known_services = ["s3", "blob"]

class ServiceAction(Action):
    def __call__(self, parser, namespace, values, option_string=None):
        service, destination = values
        if service.lower() not in known_services:
            parser.error("Unknown service. Available service are 's3' and 'blob'")
        namespace.service = service.lower()
        namespace.destination = destination

def create_parser():
    parser = ArgumentParser()
    parser.add_argument("file_path", help="Path of the file to back up")
    parser.add_argument("--service",
            help="where to store backup",
            nargs=2,
            action=ServiceAction,
            required=True)
    return parser